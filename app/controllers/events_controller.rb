class EventsController < ApplicationController

    def create
        @event = Event.create({name: params[:name]})
        render json: @event, status: 200
    end

    def search
        @events = Event.where( "name ilike '%#{params[:query]}%'").limit(10)
        render json: @events, status: 200
    end
end
