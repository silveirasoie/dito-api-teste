#require "faraday_middleware/aws_signers_v4"
ENV["ELASTICSEARCH_URL"] = "https://search-testedito-4z6ns65xbo22h4dfs76nwmwnqm.sa-east-1.es.amazonaws.com/"

Searchkick.aws_credentials = {
  access_key_id: ENV.fetch('AWS_ACCESS_KEY_ID'),
  secret_access_key: ENV.fetch('AWS_SECRET_ACCESS_KEY'),
  region: ENV.fetch('AWS_REGION')
}